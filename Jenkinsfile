// Config =========================


branch = "develop"
env.MAVEN_PROJECT_BRANCH = "${env.BRANCH_NAME}" // needed to use maven-git-versioning-extension in headless mode
short_branch = "${env.BRANCH_NAME}".replaceAll("/","-")
serviceName = 'oncologyworkflowui-fhirui'
awsCredentialsId = 'ow-deployer.creds'
awsRegion = "${env.stackregion}"
stackName ="${env.stackname}"
stackNameSpace = "${env.namespace}"
jobType = "${env.jobtype}"
dockerCredentialsId = "ecr:${awsRegion}:ow-ecr-credentials"
eksCredentialsId = "ee3b6948-5709-4548-8433-e18821d60696"
ecrUrl = 'https://923449152633.dkr.ecr.us-west-2.amazonaws.com'
owecrUrl = 'https://923449152633.dkr.ecr.us-west-2.amazonaws.com'
account = "${env.accountname}"
service = serviceName + '-microservice'
docker_image_name = serviceName
echo "docker_image_name is ${docker_image_name}"

// loadbalancer
loadbalanceUrl = ''
develop_branch = "develop" //changed to test if working with Master
release_branch = "release"
if (branch.startsWith(release_branch)) {
  ddo_branch = "${branch}"
} else {
  ddo_branch = "develop"
}

// these are OW configs details
env.STACK_NAME = "${stackName}"
env.REGION = "${awsRegion}"
env.AWS_DEFAULT_REGION = "${awsRegion}"
env.accountName = "${account}"

properties([
  disableConcurrentBuilds(),
  buildDiscarder(logRotator(daysToKeepStr: '7', numToKeepStr: '10'))
])

// Stages =========================
node ('ow-team') {
  step([$class: 'WsCleanup'])
  timestamps {
    try {
      stage('docker-login') {
        withCredentials([getUserNameMultiBindings()]) {
          def login_cmd = sh(script: "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} AWS_DEFAULT_REGION=${awsRegion} aws ecr get-login --no-include-email", returnStdout: true)
          sh "#!/bin/sh -e\n ${login_cmd}"
        }
      }
      stage('Checkout') {
        dir(service) {
          checkout()
        }
      }

      dir(service) {
        docker.image('localstack/localstack:0.8.7').withRun(
          '-e SERVICES=s3:4572,sqs:4576 ' +
            '-e DEFAULT_REGION=us-west-2 ' +
            '-e HOSTNAME_EXTERNAL=s3.localstack.navify ' +
            '--expose 4570-4576 ')
          {  l ->
           
            docker.image(ecrUrl.replace("https://","")+'/ddo-jenkins-slave-centos-saas:latest').inside(
              "--link ${l.id}:s3.localstack.navify " +
                '-v /opt/jenkins-slave/m2_repo:/root/.m2') {
        stage('build configs for webapp') {
          withCredentials([getUserNameMultiBindings()]) {
            sh "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}  AWS_DEFAULT_REGION=${awsRegion}  CLUSTER_NAME=${stackName}"
            sh 'node - v'
            sh 'npm install --save-dev aws-sdk path fs-extra '
            sh 'node scripts/build.env.configs.js'
            }
          }
        stage('Sonar Scan webapp') {
            sh 'npm install sonar-scanner --save-dev'
            sh 'npm run sonar-scanner'
            }
          }
          stage("building Image and push to ECR") {
             image_tag ="latest-${BUILD_NUMBER}"
              imageToRegistry(image_tag)
          }
     
       
      }
    }
  } catch (Exception e) {
      println("Caught exception: " + e)
      error = catchException exception: e
    } finally {
      println("CurrentBuild result: " + currentBuild.result)
      // sendSlack()
      // emailnotify()
      // stage('Publish HTML report') {
      //   publishHTML(target: [
      //     allowMissing: false,
      //     alwaysLinkToLastBuild: false,
      //     keepAll: true,
      //     reportDir: 'coverage',
      //     reportFiles: 'index.html',
      //     reportName: 'OW_Webapp_Unit_Test Report'
      //   ])
      // }
    }
  }
}

boolean shouldDeploy() {
  echo 'should deploy boolean'
  print develop_branch
  print branch
  return branch == develop_branch


}

def imageToRegistry(image_tag) {
  withCredentials([getUserNameMultiBindings()]) {
    sh(script: "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} AWS_DEFAULT_REGION=${awsRegion} infra/pipeline/scripts/create-repository.sh ${docker_image_name} ${awsRegion} ")
    docker.withRegistry(owecrUrl, awsCredentialsId) {
      def login_cmd = sh(script: "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} AWS_DEFAULT_REGION=${awsRegion} aws ecr get-login --no-include-email", returnStdout: true)
      sh "#!/bin/sh -e\n ${login_cmd}"
      new_image = docker.build(docker_image_name)
      sh "docker images ${docker_image_name}"
      new_image.push(image_tag)
    }
  }
}

def checkout() {
  checkout scm
  echo 'Pulling... ' + env.GIT_BRANCH
  def commit = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
  env.GIT_COMMIT = commit
}



def getUserNameMultiBindings(){
  return [
    $class: 'UsernamePasswordMultiBinding',
    credentialsId: awsCredentialsId,
    usernameVariable: 'AWS_ACCESS_KEY_ID',
    passwordVariable: 'AWS_SECRET_ACCESS_KEY']
}
